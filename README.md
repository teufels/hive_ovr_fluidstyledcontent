![VENDOR](https://img.shields.io/badge/vendor-HIVE-%219A83.svg)
![KEY](https://img.shields.io/badge/key-hive__ovr__fluidstyledcontent-blue.svg)
![version](https://img.shields.io/badge/version-11.2.*-yellow.svg?style=flat-square)

HIVE > Override Fluid Styled Content
==========
Overrides for Fluid Styled Content

#### This version supports TYPO3


![TYPO3Version](https://img.shields.io/badge/11_LTS-%23A6C694.svg?style=flat-square)
![TYPO3Version](https://img.shields.io/badge/12_LTS-%23A6C694.svg?style=flat-square)

### Composer support
`composer req beewilly/hive_ovr_fluidstyledcontent`

### Requirements
- `beewilly/hive_image`

### How to use
- Install with composer
- Import Static Template (before hive_thm_custom)

### Notice
- developed with mask & mask_export

### Changelog
- 11.2.2 prepare for Typo3 v12 LTS
- 11.2.0 outsource image override & svg Viewhelper in seperate EXT hive_image
- 11.1.1 add removeStyleAttributes Constant for inline SVG
- 11.1.0 remove bLazy completely (=> support 11.5 only to besure no blazy is used anywhere)
- [...see bitbucket Commits]