<?php
defined('TYPO3') || die('Access denied.');
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

/***************
 * Setup EXTKEY
 * was null on Typo3 10
 ****************/
$_EXTKEY ='hive_ovr_fluidstyledcontent';
$extKey = 'hive_ovr_fluidstyledcontent';

call_user_func(
    function($extkey)
    {

        /***************
         * Add page TSConfig
         */
        $pageTsConfig = @file_get_contents(\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($extkey) . 'Configuration/TsConfig/Page/config.txt');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig($pageTsConfig);

    },
    'hive_ovr_fluidstyledcontent'
);